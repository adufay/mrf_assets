<scene>
  <lights>
    <area_light>
      <emittance type="diffuse" name="Area.005">
        <radiance value="100.0" />
        <rgb_color r="1.0" g="1.0" b="1.0" />
        <spectrum file="./Cozy_assets/d65.spd" />
      </emittance>
      <transformations>
        <transform>
          <scale x="1.0" y="1.0" z="1.0" />
          <translation x="5.758990287780762" y="4.319242477416992" z="10.416996002197266" />
          <rotation>
            <angle value="27.683190423073974" />
            <axis x="-0.5839707851409912" y="0.8035007119178772" z="0.1156129390001297" />
          </rotation>
        </transform>
      </transformations>
    </area_light>
    <area_light>
      <emittance type="diffuse" name="Area.006">
        <radiance value="15.78852034623969" />
        <rgb_color r="1.0" g="1.0" b="1.0" />
        <spectrum file="./Cozy_assets/d65.spd" />
      </emittance>
      <transformations>
        <transform>
          <scale x="2.3399999141693115" y="2.3399999141693115" z="1.0" />
          <translation x="5.582432270050049" y="0.19623778760433197" z="-7.057794570922852" />
          <rotation>
            <angle value="146.87054127002466" />
            <axis x="-0.10733756422996521" y="0.9932871460914612" z="-0.043122295290231705" />
          </rotation>
        </transform>
      </transformations>
    </area_light>
    <sphere_light radius="0.029999999329447746">
      <emittance type="uniform" name="Point">
        <radiance value="60.0" />
        <rgb_color r="1.0" g="0.686244010925293" b="0.2833449363708496" />
        <spectrum file="./Cozy_assets/osram.spd" />
      </emittance>
      <position x="0.4186158776283264" y="1.120177984237671" z="0.0336175411939621" />
    </sphere_light>
  </lights>
  <materials>
    <material name="default_lambert" type="Lambert">
      <rgb_color r="0.8" g="0.8" b="0.8" />
      <albedo value="1.0" />
      <spectrum file="./Cozy_assets/default_lambert.spd" />
    </material>
    <material name="Chair" type="ggx" alpha_g="0.0010000000474974513">
      <fresnel>
        <eta>
          <rgb_color r="1.9236307985483683" g="1.9236307985483683" b="1.9236307985483683" />
          <spectrum file="./Cozy_assets/Chair_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="0.4828872828692469" g="0.4828872828692469" b="0.4828872828692469" />
          <spectrum file="./Cozy_assets/Chair_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material alpha_g="0.25" name="lamp-dome" type="walter_bsdf">
      <extinction>
        <rgb_color b="0.0" g="0.0" r="0.0" />
        <spectrum value="0:0.0,1000:0.0" />
      </extinction>
      <fresnel>
        <eta>
          <rgb_color b="1.45" g="1.45" r="1.45" />
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <rgb_color b="0.0" g="0.0" r="0.0" />
          <spectrum value="0:0.0,1000:0.0" />
        </kappa>
      </fresnel>
    </material>
    
    <material name="shelf-horiz" type="ggx" alpha_g="0.046">
      <fresnel>
        <eta>
          <rgb_color r="1.7507008538411801" g="1.7507008538411801" b="1.7507008538411801" />
          <spectrum file="./Cozy_assets/shelf-horiz_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="0.3239204555728948" g="0.3239204555728948" b="0.3239204555728948" />
          <spectrum file="./Cozy_assets/shelf-horiz_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material name="lamp-support" type="ggx" alpha_g="0.08">
      <fresnel>
        <eta>
          <rgb_color r="3.9999998478594816" g="3.9999998478594816" b="3.9999998478594816" />
          <spectrum file="./Cozy_assets/lamp-support_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="12649.109415235433" g="12649.109415235433" b="12649.109415235433" />
          <spectrum file="./Cozy_assets/lamp-support_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material name="Pillow" type="ggx" alpha_g="0.625">
      <fresnel>
        <eta>
          <rgb_color r="3.6777433950083256" g="1.1950400470163107" b="1.0" />
          <spectrum file="./Cozy_assets/Pillow_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="7.188435917214715" g="0.024761875471403842" b="0.0" />
          <spectrum file="./Cozy_assets/red.spd" />
        </kappa>
      </fresnel>
    </material>
    <material name="background" type="Lambert">
      <rgb_color r="0.25835490226745605" g="0.6693924069404602" b="1.0" />
      <albedo value="1.0" />
      <spectrum file="./Cozy_assets/blue.spd" />
    </material>
    <material name="lamp-ring" type="ggx" alpha_g="0.09166666865348816">
      <fresnel>
        <eta>
          <rgb_color r="1.4279672577130307" g="1.4279672577130307" b="1.4279672577130307" />
          <spectrum file="./Cozy_assets/lamp-ring_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="0.11133650574739809" g="0.11133650574739809" b="0.11133650574739809" />
          <spectrum file="./Cozy_assets/lamp-ring_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material name="lamp-base-support" type="ggx" alpha_g="0.01166666865348816">
      <fresnel>
        <eta>
          <rgb_color r="3.6777433950083256" g="2.3187310809650374" b="1.8118743419529337" />
          <spectrum file="./Cozy_assets/lamp-base-support_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="7.188435917214715" g="0.982756714679789" b="0.3764117961428973" />
          <spectrum file="./Cozy_assets/lamp-base-support_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material name="shelf-vert" type="ggx" alpha_g="0.36484387516975403">
      <fresnel>
        <eta>
          <rgb_color r="3.9999998478594816" g="3.9999998478594816" b="3.9999998478594816" />
          <spectrum file="./Cozy_assets/shelf-vert_eta.spd" />
        </eta>
        <kappa>
          <rgb_color r="12649.109415235433" g="12649.109415235433" b="12649.109415235433" />
          <spectrum file="./Cozy_assets/shelf-vert_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
  </materials>
  <shapes>
    <shape ref_material="Chair">
      <mesh>
        <obj file="./Cozy_assets/Arm rest.obj" />
      </mesh>
    </shape>
    <shape ref_material="Chair">
      <mesh>
        <obj file="./Cozy_assets/Base join.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-dome">
      <mesh>
        <obj file="./Cozy_assets/Dome.obj" />
      </mesh>
    </shape>
    <shape ref_material="shelf-horiz">
      <mesh>
        <obj file="./Cozy_assets/Horizontal.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-support">
      <mesh>
        <obj file="./Cozy_assets/Lamp base.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-support">
      <mesh>
        <obj file="./Cozy_assets/Lamp trunk.obj" />
      </mesh>
    </shape>
    <shape ref_material="Pillow">
      <mesh>
        <obj file="./Cozy_assets/Pillow.obj" />
      </mesh>
    </shape>
    <shape ref_material="background">
      <mesh>
        <obj file="./Cozy_assets/Plane.obj" />
      </mesh>
    </shape>
    <shape ref_material="Chair">
      <mesh>
        <obj file="./Cozy_assets/Rear join.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-ring">
      <mesh>
        <obj file="./Cozy_assets/Ring dome.obj" />
      </mesh>
    </shape>
    <shape ref_material="Chair">
      <mesh>
        <obj file="./Cozy_assets/Seat shell.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-ring">
      <mesh>
        <obj file="./Cozy_assets/Support axis A.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-ring">
      <mesh>
        <obj file="./Cozy_assets/Support axis B.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-ring">
      <mesh>
        <obj file="./Cozy_assets/Support axis C.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-base-support">
      <mesh>
        <obj file="./Cozy_assets/Support base.obj" />
      </mesh>
    </shape>
    <shape ref_material="lamp-ring">
      <mesh>
        <obj file="./Cozy_assets/Support dome.obj" />
      </mesh>
    </shape>
    <shape ref_material="shelf-vert">
      <mesh>
        <obj file="./Cozy_assets/Vertical.obj" />
      </mesh>
    </shape>
  </shapes>
</scene>
