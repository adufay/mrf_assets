To uses the textures referenced in the .msf file, data can be retrieved from:
https://gitlab.inria.fr/dmurray/mrf_envmap_assets
Otherwise, replace by your own.