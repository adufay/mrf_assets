emissive material
-----------------
  Blicker glow         10 -> 0
  Headlight main lamp   6 -> 0
  Headlight lamps       6 -> 0
  Sub Headlight bulb    ? -> 0
  Tail light bulbs      ? -> 0


Unknow objects
--------------

Grid lights 3
Grid lights 4
Grid Lights Bottom 3
Grid Lights Bottom 4
Raw doors
Plane
RearLight Glass.001
Rim class
Rim Empty
Sphere


Material to change:
------------------

Titanium blue super reflective:
  Headlight reflector
  Headlight lamp base

h:
  Inner healight trim

Tires:
  LB Tire
  LF Tire
  RB Tire
  RF Tire


M3 Logo:
  M3 Side logo

Tailight main
  tailight main white


Object to assign new material:
------------------------------

Mirror (rétroviseur):
  carpaint

Side pannels:
  carpaint

Under bumber:
  carpaint

Under headlight trim:
  carpaint



Interior scene:
---------------

Dimensions: 
  car: 1,88m x 4,67m
  1 blender square: 0,5m - 0,65m
  floor 30x30 -> 15m-19,5m
  Repere 1m -> 5.17
