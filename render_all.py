#!/usr/bin/env python3

import os
from subprocess import call

output_dir = 'output'
output_format = 'exr'
scenes_folder = 'scenes'
malia_rgb_path = 'malia_rgb'
malia_path = 'malia'
mic_path = 'mic'
spp = 16

def render_rgb(scene, output):
    args = [
        malia_rgb_path,
        '-spp', str(spp),
        '-scene', scene,
        '-o', output
    ]

    print(' '.join(args))
    call(args)


def render_spectral(scene, output):
    args = [
        malia_path,
        '-spp', str(spp),
        '-scene', scene,
        '-o', output
    ]

    print(' '.join(args))
    call(args)

    
def convert_rgb(image_in, image_out):
    args = [
        mic_path,
        '-in', image_in,
        '-out', image_out
    ]
    call(args)
    

if __name__ == '__main__':
    scenes = []
    renderings_rgb = []
    renderings_spectral = []

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    for (dirpath, dirnames, filenames) in os.walk(scenes_folder):
        for f in filenames:
            if f.endswith('.msf'):
                scene_path = os.path.join(dirpath, f)
                rendering_name_rgb      = f[:-4] + '_rgb.' + output_format
                rendering_name_spectral = f[:-4] + '_spectral.' + output_format
                
                rendering_path_rgb = os.path.join(output_dir, rendering_name_rgb)
                rendering_path_spectral = os.path.join(output_dir, rendering_name_spectral)
                
                render_rgb(scene_path, rendering_path_rgb)
                render_spectral(scene_path, rendering_path_spectral)

                scenes.append(scene_path)

                if output_format != 'png':
                    png_filename_rgb =  rendering_name_rgb[:-4] + '_rgb.png'
                    png_filename_spectral = rendering_name_spectral[:-4] + '_spectral.png'
                    
                    convert_rgb(rendering_path_rgb, os.path.join(output_dir, png_filename_rgb))
                    convert_rgb(rendering_path_spectral, os.path.join(output_dir, png_filename_spectral))

                    renderings_rgb.append(png_filename_rgb)
                    renderings_spectral.append(png_filename_spectral)
                else:
                    renderings_rgb.append(rendering_name_rgb)
                    renderings_spectral.append(rendering_name_spectral)

    with open(os.path.join(output_dir, 'index.html'), 'w') as f:
        f.write('<html><head>')
        f.write("""
        <style>
        body {
        max-width:1060px;
        margin:auto;
        font-family: sans-serif;
        }
        img {
        width:512px;
        margin:5px;
        }
        </style>
        """)
        f.write('</head><body>')
        f.write('<h1>Rendering report</h1>')
        f.write('<p>Rendering with ' + str(spp) + ' samples')
        for scene, rendering_rgb, rendering_spectral in zip(scenes, renderings_rgb, renderings_spectral):
            f.write('<h2>{}</h2>'.format(scene))
            f.write('  <img alt="RGB" width="512" src="{}" /><img alt="spectral" width="512" src="{}" />'.format(rendering_rgb, rendering_spectral))
            f.write('<br/>')
        f.write('</body>')
            
